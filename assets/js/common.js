// 全局的axios配置

//1. 配置基地址
axios.defaults.baseURL = 'http://www.itcbc.com:8000'

//2. 添加拦截器

//2.1 添加请求拦截器 
axios.interceptors.request.use(
  // config：请求报文信息
  function (config) {
    // 在发送请求之前做些什么
    let token = localStorage.getItem('token')
    //如果有token则通过请求头的方式传递,没有token就不发
    if (token) {
      // config.headers请求报文头
      config.headers.Authorization = token
    }
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

//2.2 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response
  },
  function (error) {
    // 响应错误了，出错了
    // 如果是状态码401,说明token认证失败( 没有token 或者 token过期导致错误都会出现401)
    if (error.response.status == 401) {
      if (window.location.href.indexOf('login') == -1) {
        alert('请重新登录')
      }
      window.parent.location.href = './login.html'
    }

    // 对响应错误做点什么
    return Promise.reject(error)
  }
)
