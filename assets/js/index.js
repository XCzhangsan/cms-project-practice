//初始化数据
document.querySelector('.init').addEventListener('click', function () {
    axios({
        url: '/init/data',
        method: 'get'
    }).then(res => {
        if (res.data.code == 0) {
            // 成功提示
            Toast.success('恭喜您，初始化数据成功！')
            // 刷新页面
            location.reload()
        } else {
            Toast.info(res.data.message)
        }
    })
})
