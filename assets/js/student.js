const initData = () => {
    axios({
        url: '/student/list',
        method: 'get'
    }).then(res => {
        // console.log(res)
        // 获取成功
        if (res.data.code == 0) {
            // 渲染数组
            document.querySelector('tbody').innerHTML = res.data.data.map(item => `
                <tr>
                    <th scope="row">${item.id}</th>
                    <td>${item.name}</td>
                    <td>${item.age}</td>
                    <td>${item.sex}</td>
                    <td>${item.group}</td>
                    <td>${item.phone}</td>
                    <td>${item.salary}</td>
                    <td>${item.truesalary}</td>
                    <td>${item.province + item.city + item.county}</td>
                    <td>
                        <button type="button" data-id="${item.id}" class="update btn btn-primary btn-sm">修改</button>
                        <button type="button" data-id="${item.id}" class="delete btn btn-danger btn-sm">删除</button>
                    </td>
                </tr>
                `).join('')
        }
    })
}

initData()

const province = document.querySelector('[name=province]')
const city = document.querySelector('[name=city]')
const county = document.querySelector('[name=county]')
const form = document.querySelector('.add-form')
const addModalLabel = document.querySelector('.addModalLabel')
const btnAddStudent = document.querySelector('.btnAddStudent')
//添加学员
document.querySelector('.btnAddStu').addEventListener('click', async () => {
    id = null
    addModalLabel.innerHTML = '添加学员信息'
    btnAddStudent.innerText = '确认添加'
    $('#addModal').modal('show')
    form.reset()
})
//渲染模态框省市区
async function setPCA() {
    const res = await axios({ url: '/geo/province' })
    province.innerHTML = '<option value="">--省--</option>' + res.data.map(item => `<option value="${item}">${item}</option>`).join('')
    province.addEventListener('change', async () => {
        const res = await axios({ url: '/geo/city', params: { pname: province.value } })
        // console.log(res);
        city.innerHTML = '<option value="">--城市--</option>' + res.data.map(item => `<option value="${item}">${item}</option>`).join('')
        county.innerHTML = '<option value="">--地区--</option>'
    })
    city.addEventListener('change', async () => {
        const res = await axios({ url: '/geo/county', params: { pname: province.value, cname: city.value } })
        // console.log(res);
        county.innerHTML = '<option value="">--地区--</option>' + res.data.map(item => `<option value="${item}">${item}</option>`).join('')
    })
}
setPCA()
//模态框中的确认添加和确认修改按钮
document.querySelector('.btnAddStudent').addEventListener('click', async e => {
    e.preventDefault()
    const data = serialize(form, { hash: true, empty: true })
    // console.log(data);
    if (id) {
        data.id = id
        await axios({
            url: '/student/update',
            method: 'put',
            data
        })
    } else {
        await axios({
            url: '/student/add',
            method: 'post',
            data
        })
    }
    // console.log(res);
    initData()
    $('#addModal').modal('hide')
})
//删除按钮
document.querySelector('tbody').addEventListener('click', async e => {
    if (e.target.classList.contains('delete')) {
        const id = e.target.dataset.id
        await axios({ url: '/student/delete', method: 'delete', params: { id } })
        initData()
    }
})
//修改按钮
let id
document.querySelector('tbody').addEventListener('click', async e => {
    if (e.target.classList.contains('update')) {
        addModalLabel.innerHTML = '编辑学员信息'
        btnAddStudent.innerText = '确认编辑'
        id = e.target.dataset.id
        // console.log(id);
        $('#addModal').modal('show')
        const res = await axios({ url: '/student/one', params: { id } })
        // console.log(res.data.data);
        const data = res.data.data
        //回显名字、年龄、组、电话、期望薪资、实际薪资
        document.querySelector('[name="name"]').value = data.name
        document.querySelector('[name="age"]').value = data.age
        document.querySelector('[name="group"]').value = data.group
        document.querySelector('[name="phone"]').value = data.phone
        document.querySelector('[name="salary"]').value = data.salary
        document.querySelector('[name="truesalary"]').value = data.truesalary
        //回显性别
        if (data.sex === '男') {
            document.querySelector('[value="男"]').checked = true
        } else {
            document.querySelector('[value="女"]').checked = true
        }
        const pRes = await axios({ url: '/geo/province' })
        // 渲染省份
        // console.log(pRes);
        province.innerHTML = `<option selected value="">--省份--</option>` + pRes.data.map(item => `<option value="${item}">${item}</option>`).join('')

        // 获取所有城市
        const cRes = await axios({ url: '/geo/city', params: { pname: data.province } })
        city.innerHTML = `<option selected value="">--城市--</option>` + cRes.data.map(item => `<option value="${item}">${item}</option>`).join('')

        // 获取所有地区
        const aRes = await axios({ url: '/geo/county', params: { pname: data.province, cname: data.city } })
        county.innerHTML = `<option selected value="">--地区--</option>` + aRes.data.map(item => `<option value="${item}">${item}</option>`).join('')
    }
    //回显省市县
    // document.querySelector('[name="province"]').innerHTML = `<option selected value="${data.province}">${data.province}</option>`
    // document.querySelector('[name="city"]').innerHTML = `<option selected value="${data.city}">${data.city}</option>`
    // document.querySelector('[name="county"]').innerHTML = `<option selected value="${data.county}">${data.county}</option>`
})
