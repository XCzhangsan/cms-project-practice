const loginBox = document.querySelector('.login')
const registerBox = document.querySelector('.register')
document.querySelector('.goRegister').addEventListener('click', () => {
    loginBox.style.display = 'none'
    registerBox.style.display = 'block'
})
document.querySelector('.goLogin').addEventListener('click', () => {
    registerBox.style.display = 'none'
    loginBox.style.display = 'block'
})
const registerForm = document.querySelector('.register-form')
const loginForm = document.querySelector('.login-form')
const toast = new bootstrap.Toast('#myToast')
const toastBody = document.querySelector('.toast-body')
//注册事件
document.querySelector('.register-btn').addEventListener('click', async e => {
    e.preventDefault()
    const data = serialize(registerForm, { hash: true, empty: true })
    // console.log(data);
    if (data.username.length < 6 || data.username.length > 15) {
        toastBody.innerHTML = '用户名必须是6-15个字符'
        toast.show()
        return
    }
    if (data.password.length < 6 || data.password.length > 15) {
        toastBody.innerHTML = '密码必须是6-15个字符'
        toast.show()
        return
    }
    const res = await axios({
        url: 'http://www.itcbc.com:8000/api/register',
        method: 'post',
        data: data
    })
    console.log(res)
    toastBody.innerHTML = res.data.message
    toast.show()
    if (!res.data.code) {
        setTimeout(() => {
            registerBox.style.display = 'none'
            loginBox.style.display = 'block'
        }, 1000)
    }
})
//登录事件
document.querySelector('.login-btn').addEventListener('click', async e => {
    e.preventDefault()
    const data = serialize(loginForm, { hash: true, empty: true })
    // console.log(data);
    if (data.username.length < 6 || data.username.length > 15) {
        toastBody.innerHTML = '用户名必须是6-15个字符'
        toast.show()
        return
    }
    if (data.password.length < 6 || data.password.length > 15) {
        toastBody.innerHTML = '密码必须是6-15个字符'
        toast.show()
        return
    }
    const res = await axios({
        url: 'http://www.itcbc.com:8000/api/login',
        method: 'post',
        data: data
    })
    console.log(res)
    toastBody.innerHTML = res.data.message
    toast.show()
    localStorage.setItem('token', res.data.token)
    setTimeout(() => {
        location.href = './index.html'
    }, 1000)
})