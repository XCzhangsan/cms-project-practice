//判断是否有token
const token = localStorage.getItem('token')
//没有就返回登录页
if (!token) location.href = './login.html'
//渲染用户名

//退出登录
document.querySelector('.logout').addEventListener('click', () => {
    // //清楚token和用户名
    // localStorage.removeItem('token')
    // localStorage.removeItem('username')
    // //清楚本地所有数据
    localStorage.clear()
    location.href = './login.html'
})