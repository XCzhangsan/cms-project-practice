async function scoreList() {
    const res = await axios({
        url: '/score/list',
    })
    console.log(res)
    const data = res.data.data
    console.log(data)
    document.querySelector('tbody').innerHTML = Object.keys(data).map(item => `<tr>
    <th scope="row">${item}</th>
    <td>${data[item].name}</td>
    <td class="score">${data[item].score[0]}</td>
    <td class="score">${data[item].score[1]}</td>
    <td class="score">${data[item].score[2]}</td>
    <td class="score">${data[item].score[3]}</td>
    <td class="score">${data[item].score[4]}</td>
  </tr>`).join('')
}
scoreList()

document.querySelector('tbody').addEventListener('click', e => {
    if (e.target.classList.contains('score')) {
        console.log(1);
    }
})